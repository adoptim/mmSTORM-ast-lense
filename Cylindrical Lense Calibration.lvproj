﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="LenseCalibrationGUI.vi" Type="VI" URL="../LenseCalibrationGUI.vi"/>
		<Item Name="LenseCalibrationRegGUI.vi" Type="VI" URL="../LenseCalibrationRegGUI.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="matrixMultiply.vi" Type="VI" URL="../matrixMultiply.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="LenseCalibrationGUI" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{495BE17A-D2B3-44DB-A34F-D860663A8DD6}</Property>
				<Property Name="App_INI_GUID" Type="Str">{011BE644-3064-46DF-BB6D-99328CC8E6FA}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{01CBBD9B-6D41-4DFF-9F2E-0B377820DB60}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">LenseCalibrationGUI</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{0E773720-D39A-4EBB-BA7A-9E3D87C9B22F}</Property>
				<Property Name="Bld_version.build" Type="Int">3</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">LenseCalibrationGUI.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/LenseCalibrationGUI.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{54DB6B67-9497-46D5-B50A-C3D5F8342948}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/LenseCalibrationGUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">University of Szeged</Property>
				<Property Name="TgtF_fileDescription" Type="Str">LenseCalibrationGUI</Property>
				<Property Name="TgtF_internalName" Type="Str">LenseCalibrationGUI</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2018 University of Szeged</Property>
				<Property Name="TgtF_productName" Type="Str">LenseCalibrationGUI</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{785F9106-AA10-43DE-9F8C-BD048515F644}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">LenseCalibrationGUI.exe</Property>
			</Item>
			<Item Name="RegularLenseCalibrarionGUI" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{E54029B6-365D-4556-AEBF-EF33570C53AC}</Property>
				<Property Name="App_INI_GUID" Type="Str">{EA2E6354-B8FB-44D9-A43E-86904B634D6B}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{370CA657-37C6-4CF7-9A07-AA1BD941F2C7}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">RegularLenseCalibrarionGUI</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{0125B790-90B0-4F42-AEA2-A5F6BD49FDF0}</Property>
				<Property Name="Bld_version.build" Type="Int">2</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">RegularLenseCalibrarionGUI.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/RegularLenseCalibrarionGUI.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{96B1455E-D99F-4EE1-9AF8-F92766035182}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/LenseCalibrationRegGUI.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">University of Szeged</Property>
				<Property Name="TgtF_fileDescription" Type="Str">RegularLenseCalibrarionGUI</Property>
				<Property Name="TgtF_internalName" Type="Str">RegularLenseCalibrarionGUI</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2018 University of Szeged</Property>
				<Property Name="TgtF_productName" Type="Str">RegularLenseCalibrarionGUI</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{C5108FED-ED47-4499-A54C-FAB8778AF561}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">RegularLenseCalibrarionGUI.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
